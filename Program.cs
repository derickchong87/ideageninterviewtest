﻿using System;
using System.Data;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string expression = Console.ReadLine();

            Console.WriteLine(Calculate(expression).ToString());

        }

        /// <summary>
        /// This is to calculate the expressions if they have brackets
        /// </summary>
        /// <param name="inputExpression"></param>
        /// <returns></returns>
        public static string HandleBrackets(string inputExpression) {

            return Calculate(inputExpression.Trim()).ToString();
            
        }

        /// <summary>
        /// Start of the function
        /// </summary>
        /// <param name="sum"></param>
        /// <returns></returns>
        public static double Calculate(string sum) {

            if (sum.Contains("("))
            {
                int? firstBracket = null;
                int? lastBracket = null;

                firstBracket = sum.IndexOf("(");

                lastBracket = sum.LastIndexOf(")");

                string filteredExpression = sum.Substring(firstBracket.Value + 2, lastBracket.Value - firstBracket.Value - 2);

                string processedExpression = HandleBrackets(filteredExpression);

                sum = sum.Remove(firstBracket.Value, lastBracket.Value - firstBracket.Value + 1);

                sum = sum.Insert(firstBracket.Value, processedExpression);

            }

            string[] input = sum.Trim().Split(" ");

            double output = 0;

            for(int i = 0;i < input.Length; i++)
            {
                if (i == 0)
                {
                    output = double.Parse(input[i]);
                }

                // Only when it is the operator
                if (i % 2 > 0)
                {
                    double secondNumber = double.Parse(input[i + 1]);

                    output = Math.Round(Process(input[i], output, secondNumber), 2);

                }
                
            }

            return output;
        }


        public static double Process(string expression, double num1, double num2) {
            double output = num1;

            if (expression.Equals("*"))
            {
                output = output * num2;
            }
            else if (expression.Equals("/"))
            {
                output = output / num2;
            }
            else if (expression.Equals("+"))
            {
                output = output + num2;
            }
            else if (expression.Equals("-"))
            {
                output = output - num2;
            }

            return output;

        }
        
    }
}
